import scipy
import numpy as np
import soundfile as sf
import matplotlib.pyplot as plt

PROJECT_FOLDER = '/Users/ruadld6/Documents/dsp'


def deconvolve(x, y):
	print(x.shape)
	print(x[:10])
	print(y.shape)
	y = y[:len(x)]
	print(x.shape)
	print(y.shape)
	X = np.fft.rfft(x[:100000])
	plt.plot(X)
	plt.show()
	Y = np.fft.rfft(y)
	H = Y/X
	h = np.fft.irfft(H)
	return h


orig, sr = sf.read(f'{PROJECT_FOLDER}/records/sweeper.wav')
reverb, _ = sf.read(f'{PROJECT_FOLDER}/records/recorded_sweeper.wav')

h = deconvolve(orig[:, 0], reverb)

# print(h)

plt.plot(h)
plt.show()

restore_rev = scipy.signal.fftconvolve(orig, h, 'full')[:len(orig)]
sf.write(f'{PROJECT_FOLDER}/records/cutted_recorded_sweeper.wav', restore_rev, sr)